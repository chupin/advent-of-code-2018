{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable #-}

module Stream where

import Prelude hiding (cycle, iterate, iterate', zip, zipWith)

data Stream a = a ::: Stream a
              deriving(Show, Functor, Foldable, Traversable)

cycle :: [a] -> Stream a
cycle xs = ys
  where ys = foldr (:::) ys xs

foldStream :: (a -> b -> b) -> Stream a -> b
foldStream f (x ::: xs) = f x (foldStream f xs)

iterate :: (a -> a) -> a -> Stream a
iterate f x = x ::: iterate f (f x)

iterate' :: (a -> a) -> a -> Stream a
iterate' f x = x' `seq` (x ::: iterate' f x')
  where x' = f x

zipWith :: (a -> b -> c) -> Stream a -> Stream b -> Stream c
zipWith f (x ::: xs) (y ::: ys) = f x y ::: zipWith f xs ys

zip :: Stream a -> Stream b -> Stream (a, b)
zip = zipWith (,)

nats :: Num a => Stream a
nats = iterate' (1 +) 0
