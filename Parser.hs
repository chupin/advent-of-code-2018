{-# LANGUAGE LambdaCase #-}

module Parser where

import Control.Applicative
import Control.Arrow

newtype Parser s a = Parser { stepParser :: [s] -> [(a, [s])] }

instance Functor (Parser s) where
  fmap f (Parser p) = Parser (fmap (first f) . p)

instance Applicative (Parser s) where
  pure x = Parser $ \s -> [(x, s)]
  Parser pf <*> Parser px =
    Parser $ \s -> do
    (f, s) <- pf s
    (x, s) <- px s
    pure (f x, s)

instance Alternative (Parser s) where
  empty = Parser (const [])
  Parser px <|> Parser py =
    Parser $ \s ->
    case px s of
      []  -> py s
      res -> res

instance Monad (Parser s) where
  Parser px >>= mpf =
    Parser $ \s -> do
    (a, s) <- px s
    stepParser (mpf a) s

exact :: Eq s => s -> Parser s s
exact s =
  Parser $ \case
  (i : is) | i == s -> [(i, is)]
  _        -> []

exacts :: Eq s => [s] -> Parser s [s]
exacts [] = pure []
exacts (s : ss) = do
  s <- exact s
  ss <- exacts ss
  pure (s : ss)

oneOf :: Eq s => [s] -> Parser s s
oneOf []       = empty
oneOf (s : ss) = exact s <|> oneOf ss

any :: Parser s s
any = Parser $ \case
  (i : is) -> [(i, is)]
  [] -> []

runParser :: Parser s a -> [s] -> Maybe a
runParser (Parser p) s =
  case p s of
    [(r, [])] -> Just r
    _         -> Nothing
