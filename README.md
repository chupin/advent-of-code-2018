My solutions for the [https://adventofcode.com/2018/day/1](Advent of
code 2018). It should be mostly in Haskell, though some Rust or OCaml
might sneak in ;)

My programs assume the input files are in a directory `input/`.

Some modules are not solutions to problems but data structures that
might be useful for several problems. Currently these are the
[`Stream`](Stream.hs), [`Dict`](Dict.hs) and [`MMap`](MMap.hs).

For day 9, I depend on
[writer-cps-mtl](https://hackage.haskell.org/package/writer-cps-mtl)
to have a somewhat efficient writer monad.
