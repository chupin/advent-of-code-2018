{-# LANGUAGE OverloadedLists #-}

module Day02 where

import           Data.Foldable
import           Data.List     (intercalate)
import           Data.Map      (Map)
import qualified Data.Map      as M
import           Data.Monoid
import           Dict          (Dict (..))
import qualified Dict          as D

-- Counts the number of occurences of elements in a list
countOcc :: Ord c => [c] -> Map c Int
countOcc = foldr (\c cs -> M.insertWith (+) c 1 cs) []

-- The « unreduced » checksum. It is a monoid for addition.
data ChkSum = ChkSum { twos   :: Int
                     , threes :: Int
                     }

instance Semigroup ChkSum where
  ChkSum a c <> ChkSum b d = ChkSum (a + b) (c + d)

instance Monoid ChkSum where
  mempty = ChkSum 0 0

-- Compute the individual contribution of a string to the checksum
stringCheckSum :: String -> ChkSum
stringCheckSum str =
  ChkSum { twos = fromEnum (2 `elem` letters)
         , threes = fromEnum (3 `elem` letters)
         }
  where letters = countOcc str

checksum :: ChkSum -> Int
checksum (ChkSum twos threes) = twos * threes

part1 :: String -> Int
part1 = checksum . foldMap stringCheckSum . lines

-- Looks for a word differing from the input by at most one character
-- in the same position. Returns a tuple containing the head the words
-- share, the differing character and the tail the words share.
--
-- A word is a solution if either its tail cs is a solution using the
-- dictionnary below c *or* if c is the character differing from the
-- solution, in which case cs must be found in a dictionnary from a
-- character different from c
findCloseWord :: Ord c => [c] -> Dict c -> Maybe ([c], c, [c])
findCloseWord [] _ = Nothing
findCloseWord (c : cs) (Dict dict) =
  -- There are two possibilities : either c is in the head of the word
  -- we are looking for (we haven't reached the character our words
  -- don't share). Thus we recurse on the dictionnary below c. Or c is
  -- the different character. If this is the case, it means that cs
  -- must be found in one of the dictionnary below an other character
  case stillInTheHead of
    Just (h, d, t) -> Just (c : h, d, t)
    Nothing ->
      M.foldrWithKey (\c' dict acc -> if cs `D.member` dict
                                      then Just ([], c', cs)
                                      else acc) Nothing (M.delete c dict)

  where stillInTheHead =
          case M.lookup c dict of
            Just dict -> findCloseWord cs dict
            Nothing   -> Nothing

part2 :: String -> Maybe String
part2 input = fmap (\(h, _, t) -> h ++ t) result
  where ids = lines input
        dict = foldr D.insert mempty ids
        result = foldr (\id acc ->
                           case findCloseWord id dict of
                             Nothing  -> acc
                             Just res -> Just res) Nothing ids

main :: IO ()
main = do
  input <- readFile "input/day02"
  print (part1 input)
  print (part2 input)
