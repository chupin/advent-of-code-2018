{-# LANGUAGE OverloadedLists #-}

module Day01 where

import qualified Data.Set as S
import           Prelude  hiding (cycle)
import           Stream

parseInstr :: String -> Int
parseInstr ('+' : num) = read num
parseInstr num         = read num

part1 :: String -> Int
part1 = sum . fmap parseInstr . lines

cumulativeSum :: Num a => Stream a -> Stream a
cumulativeSum = go 0
  where go s (x ::: xs) = s' ::: go s' xs
          where s' = x + s

searchFirstDuplicate :: Ord a => Stream a -> a
searchFirstDuplicate = go []
  where go seen (x ::: xs)
          | x `S.member` seen = x
          | otherwise = go (S.insert x seen) xs

part2 :: String -> Int
part2 = searchFirstDuplicate . cumulativeSum . fmap parseInstr . cycle . lines

main :: IO ()
main = do
  input <- readFile "input/day01"
  print (part1 input)
  print (part2 input)
