{-# LANGUAGE GeneralizedNewtypeDeriving, OverloadedLists,
             UndecidableInstances #-}

module MMap where

import           Data.Map (Map)
import qualified Data.Map as M
import           GHC.Exts

newtype MMap k v = MMap (Map k v)
                 deriving(IsList)

instance (Ord k, Semigroup v) => Semigroup (MMap k v) where
  MMap p <> MMap q = MMap (M.unionWith (<>) p q)

instance (Ord k, Monoid v) => Monoid (MMap k v) where
  mempty = MMap []
