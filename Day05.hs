{-# LANGUAGE OverloadedLists #-}

module Day05 where

import           Data.Char
import           Data.Foldable
import           Data.Map      (Map)
import qualified Data.Map      as M
import           Data.Ord

cancelOut :: Char -> Char -> Bool
cancelOut a b =
  (isLower a && isUpper b || isUpper a && isLower b) && toLower a == toLower b

scan :: [Char] -> [Char]
scan = foldr go []
  where go a (b : bs) | cancelOut a b = bs
        go a bs       = a : bs

part1 :: String -> Int
part1 = length . scan

sameUnit :: Char -> Char -> Bool
sameUnit a b = toLower a == toLower b

shortestPossiblePolymer :: [Char] -> [Char]
shortestPossiblePolymer input =
  minimumBy (comparing length) (fmap scanFilt units)
  where units = ['a'..'z'] :: [Char]
        scanFilt c = scan $ filter (not . sameUnit c) input

part2 :: String -> Int
part2 = length . shortestPossiblePolymer

main :: IO ()
main = do
  input <- filter isLetter <$> readFile "input/day05"
  print (part1 input)
  print (part2 input)
