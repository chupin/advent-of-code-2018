{-# LANGUAGE FlexibleContexts, OverloadedLists #-}

module Day07 where

import           Data.Foldable
import           Data.Map      (Map)
import qualified Data.Map      as M
import           Data.Set      (Set)
import qualified Data.Set      as S

import           Debug.Trace

---------------------------------------------------------------------------
-- « Parsing »
---------------------------------------------------------------------------

parseDep :: String -> (Char, Char)
parseDep input = (head aft, head bef)
  where ("Step" : bef : "must" : "be" : "finished" : "before" : "step" : aft : _) =
          words input

parseDeps :: String -> Map Char (Set Char)
parseDeps = foldr addDep [] . fmap parseDep . lines
  where addDep (aft, bef) deps =
          M.insertWith (<>) bef [] $ M.insertWith (<>) aft [bef] deps

---------------------------------------------------------------------------
-- Topological sort a graph of dependencies.
--
-- A function selects what values to push on the sorted result and
-- what nodes should be kept for further sorting. The kept nodes may
-- be structurally different from their equivalent (wrt the Eq and Ord
-- instances, see part 2) in the graph, which is why the ordering of
-- unions matters.
---------------------------------------------------------------------------

topo :: (Monoid p, Ord k) => (Set k -> (p, Set k)) -> Map k (Set k) -> p
topo order graph
  | M.null graph = mempty
  | otherwise = pushed <> topo order remainingGraph
  where leaves = M.keysSet $ M.filter S.null graph
        (pushed, kept) = order leaves
        dropped = leaves S.\\ kept
        remainingGraph =
          M.fromSet (const []) kept `M.union`
          fmap (S.\\ dropped) (graph `M.withoutKeys` leaves)

part1 :: String -> [Char]
part1 = topo select . parseDeps
  where select leaves = ([sel], kept)
          where (sel, kept) = S.deleteFindMin leaves

-- Tags a task with the duration it takes to complete. The duration of
-- a task is not a distincting criterion between two LastsFor a for
-- the Eq and Ord instances
data LastsFor a = LastsFor { value    :: a
                           , lastsFor :: Int
                           }

instance Eq a => Eq (LastsFor a) where
  LastsFor { value = v1 } == LastsFor { value = v2 } = v1 == v2

instance Ord a => Ord (LastsFor a) where
  LastsFor { value = v1 } `compare` LastsFor { value = v2 } = v1 `compare` v2

-- Subtracts some time from the duration a task takes
subDur :: LastsFor a -> Int -> LastsFor a
subDur LastsFor { value = v
                , lastsFor = d
                } s = LastsFor { value = v
                               , lastsFor = d - s
                               }

-- Initial durations for the tasks in part 2
initialDuration :: Char -> LastsFor Char
initialDuration c = LastsFor { value = c
                             , lastsFor = 61 + (fromEnum c - fromEnum 'A')
                             }

durationGraph :: Map Char (Set Char)
              -> Map (LastsFor Char) (Set (LastsFor Char))
durationGraph =
  M.mapKeys initialDuration . fmap (S.map initialDuration)

-- Schedules works between n workers. It creates a task that lasts the
-- minimal duration required for the shortest task to complete. Then
-- the uncompleted tasks are pushed back in the graph with updated durations
schedule :: Ord k
         => Int
         -> Set (LastsFor k)
         -> ([(Int, Set k)], Set (LastsFor k))
schedule workers tasks =
  ([(dur, S.map value toSchedule)], forLater <> unfinished)
  where (toSchedule, forLater) = S.splitAt workers tasks
        dur = minimum (S.map lastsFor toSchedule)
        unfinished = S.filter ((> 0) . lastsFor) $ S.map (`subDur` dur) toSchedule

part2 :: String -> Int
part2 = sum . fmap fst . topo (schedule 5) . durationGraph . parseDeps

main :: IO ()
main = do
  input <- readFile "input/day07"
  print (part1 input)
  print (part2 input)
