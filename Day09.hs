{-# LANGUAGE BangPatterns, FlexibleContexts, OverloadedLists,
             TemplateHaskell #-}

module Main where

import Control.Monad.State
import Control.Monad.Writer.CPS
import Data.List                (foldl')
import Data.Map                 (Map)
import Data.Monoid              (Sum (..))
import Data.Sequence            (Seq (..))
import MMap

import Debug.Trace

type Player = Int
type Marble = Int
type Score = Sum Int

infixl 3 :<>:
data Game = !Marble :<>: !(Seq Marble)
          deriving(Show)

data Dir = ClockWise | AntiClockwise

rotate :: Game -> Dir -> Game
rotate game@(_ :<>: Empty) _                      = game
rotate (curr :<>: next :<| marbles) ClockWise     = next :<>: marbles :|> curr
rotate (curr :<>: marbles :|> next) AntiClockwise = next :<>: curr :<| marbles

rotateSome :: Game -> [Dir] -> Game
rotateSome = foldl' rotate

turn :: ( MonadState Game m
        , MonadWriter (MMap Player Score) m
        )
     => Player
     -> Marble
     -> m ()
turn player marble
  | marble `mod` 23 == 0 = do
      game <- get
      let curr :<>: next :<| marbles =
            rotateSome game (replicate 7 AntiClockwise)
      put (next :<>: marbles)
      let !score = curr + marble
      tell [(player, Sum score)]
turn _ marble = do
  game <- get
  let curr :<>: marbles = rotateSome game [ClockWise, ClockWise]
  put (marble :<>: curr :<| marbles)

play :: [Player] -> [Marble] -> Map Player Score
play players marbles = scores
  where playM = sequence_ (zipWith turn (cycle players) marbles)
        MMap scores = evalState (execWriterT playM) (0 :<>: [])

execInput :: (Player -> Marble -> r) -> String -> r
execInput cont input = cont (read players) (read lastMarble)
  where ( players : "players;" : "last" : "marble" : "is" : "worth" : lastMarble : _) =
          words input

part1 :: String -> Int
part1 input = score
  where Sum score = maximum $ execInput (\pl mr -> play [1..pl] [1..mr]) input

part2 :: String -> Int
part2 input =
  getSum $ maximum $ execInput (\pl mr -> play [1..pl] [1..100 * mr]) input

main :: IO ()
main = do
  input <- readFile "input/day09"
  print (part1 input)
  print (part2 input)
