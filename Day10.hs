{-# LANGUAGE OverloadedLists #-}

module Main where

import           Control.Monad
import           Data.Foldable
import           Data.Ord
import           Data.Set      (Set)
import qualified Data.Set      as S
import           Parser
import           Stream        (Stream (..))
import qualified Stream        as S

import           Debug.Trace

data Pos = Pos Int Int
         deriving(Eq, Ord, Show)

data Vel = Vel Int Int
         deriving(Eq, Ord, Show)

data Light = Light { pos :: Pos
                   , vel :: Vel
                   }
           deriving(Show)

step :: Light -> Light
step (Light (Pos px py) vel@(Vel vx vy)) =
  Light (Pos (px + vx) (py + vy)) vel

parseLight :: String -> Light
parseLight input = Light (Pos px py) (Vel vx vy)
  where pvec input = (read px, read py, is3)
          where (_, '<' : is1) = span (/= '<') input
                (px, ',' : is2) = span (/= ',') is1
                (py, '>' : is3) = span (/= '>') is2

        (px, py, is) = pvec input
        (vx, vy, _) = pvec is

draw :: [Light] -> (Int, String)
draw lights =
  ( (ty - by) * (tx - bx)
  , unlines [ [ if Pos px py `S.member` chart then '#' else '.'
              | px <- [bx..tx]
              ]
            | py <- [by..ty]
            ]
  )
  where chart =
          foldr (\(Light pos _) chart -> S.insert pos chart) [] lights

        (xs, ys) =
          foldr (\(Pos x y) (xs, ys) -> (S.insert x xs, S.insert y ys))
          ([], []) chart

        bx = minimum xs
        tx = maximum xs
        by = minimum ys
        ty = maximum ys

-- Returns the first local minimum in a stream
takeLastDec :: Ord a => (a -> a -> Ordering) -> Stream a -> a
takeLastDec cmp (x ::: xs@(y ::: _)) =
  case cmp x y of
    LT -> x
    _  -> takeLastDec cmp xs

-- Assumes the area occupied by the lights decreases until it reaches
-- the point where the message appears. Returns the string printing
-- the message and the number of steps necessary to reach it
pickSmallest :: String -> (Int, String)
pickSmallest input = (stps, drw)
  where lights = fmap parseLight (lines input)
        allLightsEver = S.iterate (fmap step) lights
        (stps, (_, drw)) =
          takeLastDec (comparing (fst . snd)) $
          S.zip S.nats (fmap draw allLightsEver)

main :: IO ()
main = do
  input <- readFile "input/day10"
  let (stps, str) = pickSmallest input
  print stps
  putStr str
