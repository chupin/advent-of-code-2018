{-# LANGUAGE GADTs, OverloadedLists, RecordWildCards #-}

module Day04 where

import           Control.Applicative
import           Control.Monad
import           Data.Foldable
import           Data.Map            (Map)
import qualified Data.Map            as M
import           Data.Maybe
import           Data.Ord
import           Data.Sequence       (Seq (..))
import           Data.Set            (Set)
import qualified Data.Set            as S
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.LocalTime
import           Text.Parsec         hiding ((<|>))

type Parser = Parsec String ()

type Id = Int

data Event = BeginShift Id
           | FallsAsleep
           | WakesUp
           deriving(Show)

parseInt :: Parser Int
parseInt = read <$> some digit

parseDay :: Parser Day
parseDay = do
  year <- parseInt
  _ <- char '-'
  month <- parseInt
  _ <- char '-'
  day <- parseInt
  pure (fromGregorian (fromIntegral year) month day)

parseTimeOfDay :: Parser TimeOfDay
parseTimeOfDay = do
  hour <- parseInt
  _ <- char ':'
  minute <- parseInt
  pure TimeOfDay { todHour = hour
                 , todMin = minute
                 , todSec = 0
                 }

parseId :: Parser Id
parseId = do
  _ <- char '#'
  parseInt

parseBeginShift :: Parser Event
parseBeginShift = do
  _ <- string "Guard"
  spaces
  id <- parseId
  spaces
  _ <- string "begins shift"
  pure (BeginShift id)

parseEvent :: Parser Event
parseEvent =
  parseBeginShift <|>
  (WakesUp <$ string "wakes up") <|>
  (FallsAsleep <$ string "falls asleep")

parseLog :: Parser (LocalTime, Event)
parseLog = do
  _ <- char '['
  day <- parseDay
  spaces
  tod <- parseTimeOfDay
  _ <- char ']'
  spaces
  event <- parseEvent
  pure (LocalTime day tod, event)

parseEventLog :: String -> Maybe (Map LocalTime Event)
parseEventLog str =
  foldM (\log str ->
           case parse parseLog "" str of
             Left _            -> Nothing
             Right (time, evt) -> pure (M.insert time evt log)) [] (lines str)

data Ivl = Ivl LocalTime LocalTime
         deriving(Eq, Ord, Show)

subMinute :: LocalTime -> LocalTime
subMinute (LocalTime day tod) =
  LocalTime day (timeToTimeOfDay (timeOfDayToTime tod - secondsToDiffTime 60))

data TimeDiv = TimeDiv { timesAsleep :: Set Ivl
                       , timesAwake  :: Set Ivl
                       }

asleep :: TimeDiv -> LocalTime -> LocalTime -> TimeDiv
asleep tdiv@TimeDiv { timesAsleep = asleep } start end =
  tdiv { timesAsleep = S.insert (Ivl start end) asleep }

awake :: TimeDiv -> LocalTime -> LocalTime -> TimeDiv
awake tdiv@TimeDiv { timesAwake = awake } start end =
  tdiv { timesAwake = S.insert (Ivl start end) awake }

extSchedule :: Id
            -> (TimeDiv -> LocalTime -> LocalTime -> TimeDiv)
            -> Schedule
            -> LocalTime
            -> LocalTime
            -> Schedule
extSchedule idx ext sched start end =
  M.alter (\tdiv -> Just (ext (fromMaybe (TimeDiv [] []) tdiv) start end)) idx sched

endDay :: Day -> LocalTime
endDay day = LocalTime day TimeOfDay { todHour = 1
                                     , todMin = 0
                                     , todSec = 0
                                     }

type Schedule = Map Id TimeDiv

action :: (LocalTime -> Schedule)
       -> LocalTime
       -> Event
       -> State Id (LocalTime -> Schedule)
action close start@(LocalTime day (TimeOfDay h _ _)) evt =
  case evt of
    WakesUp -> do
      pid <- get
      pure $ extSchedule pid awake (close start) start
    FallsAsleep -> do
      pid <- get
      pure $ extSchedule pid asleep (close start) start
    BeginShift nid -> do
      put nid
      pure $ extSchedule nid awake (close (endDay prevDay)) start)
      where prevDay
              | h == 0 = addDays (-1) day
              | otherwise = day

computeSchedule :: Map LocalTime Event -> Schedule
computeSchedule eventLog =
  (snd (M.foldlWithKey action (error "Unknown first guard", \_ -> []) eventLog))
  (error "Irrelevant initial time")

sleepTime :: TimeDiv -> DiffTime
sleepTime TimeDiv { timesAsleep = sleeps } =
  sum (S.map (\(Ivl (LocalTime _ t1) (LocalTime _ t2)) ->
                timeOfDayToTime t2 - timeOfDayToTime t1) sleeps)

diffTimeToMinutes :: DiffTime -> Integer
diffTimeToMinutes = (`div` 60) . (* 10^12) . diffTimeToPicoseconds

bestSleeper :: Schedule -> Id
bestSleeper divs =
  fst $ maximumBy (comparing snd) $ M.toList (fmap sleepTime divs)

sleepingOn :: TimeDiv -> Map Int Int
sleepingOn TimeDiv { timesAsleep = sleeping }
  where

part1 :: String -> Maybe Integer
part1 input =
  case parseEventLog input of
    Just eventLog ->
      where schedule = computeSchedule eventLog
            bslp = bestSleeper schedule

main :: IO ()
main = do
  input <- readFile "input/day04"
  print (parseEventLog input)
  print (part1 input)
