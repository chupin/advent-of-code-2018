{-# LANGUAGE OverloadedLists #-}

module Main where

import           Data.Foldable
import           Data.Map      (Map)
import qualified Data.Map      as M
import           Data.Ord
import           Data.Sequence (Seq (..))
import           Data.Set      (Set)
import qualified Data.Set      as S

import           Debug.Trace

-- Order matters for Ord instance and convex hull alg.
data Point = Point { xCoord :: Int
                   , yCoord :: Int
                   }
           deriving(Eq, Ord, Show)

taxiDistance :: Point -> Point -> Int
taxiDistance (Point x1 y1) (Point x2 y2) = abs (x1 - x2) + abs (y1 - y2)

type Claimed = Bool

type Grid = Map Point Claimed

-- neighbours :: Point a -> [Point a]
-- neighbours (Point x y) =
--   [ Point a b
--   | a <- [x - 1..x + 1]
--   , b <- [y - 1..y + 1]
--   , a /= b
--   ]

splitAlong :: Point -> Point -> Set Point -> (Set Point, Set Point)
splitAlong p@(Point px py) q@(Point qx qy) pts =
  S.partition split (S.delete p (S.delete q pts))
  where split (Point x y) =
          (qx - px) * (y - py) - (qy - py) * (x - px) >= 0

findHull :: Set Point -> Point -> Point -> Set Point
findHull points p q
  | S.null points = []
  | otherwise = S.insert c (findHull outpc p c <> findHull outcq c q)
    where c = maximumBy (comparing (taxiDistance p)) points
          (outpc, remngs) = splitAlong p c points
          (outcq, _) = splitAlong c q remngs

quickHull :: Set Point -> Set Point
quickHull points = [a, b] <> findHull above a b <> findHull below b a
  where a = minimumBy (comparing xCoord) points
        b = maximumBy (comparing xCoord) points
        (above, below) = splitAlong a b points

data Voronoi = Voronoi { convexHull     :: Set Point
                       , finitePatches  :: Map Point (Set Point)
                       , disputedPoints :: Set Point
                       }

-- A circle with the taxi distance
circle :: Point -> Int -> Set Point
circle (Point x y) r =
  seg <> S.map symx seg <> S.map symy seg <> S.map (symx . symy) seg
  where rs = [0..r] :: [Int]
        seg =
          foldr (\t ss -> S.insert (Point (x + r - t) (y + t)) ss) [] rs

        symx (Point a b) = Point (2 * x - a) b
        symy (Point a b) = Point a (2 * y - b)

growingSeeds :: Map Point (Set Point) -> Set Point
growingSeeds = M.keysSet . M.filter (not . S.null)

printPts :: Foldable t => t Point -> String
printPts = foldr (\(Point x y) ss -> show x ++ "," ++ show y ++ "\n" ++ ss) []

voronoi :: Set Point -> Voronoi
voronoi points = go [] [] [] points 0
  where hull = quickHull points
        go claimedPoints disputedPoints finitePatches seeds radius
          | traceShow seeds $ all (`S.member` hull) remainingSeeds =
              Voronoi { convexHull = hull
                      , finitePatches = newFinitePatches
                      , disputedPoints = newDisputedPoints
                      }
          | otherwise =
              go newClaimedPoints newDisputedPoints newFinitePatches remainingSeeds (radius + 1)

          where circlesAroundSeed =
                  M.fromSet (\p -> circle p radius S.\\ claimedPoints) seeds

                (newDisputedPoints, newClaimedPoints) =
                  foldr (\claim (disputed, claims) ->
                           ( disputed <> claim `S.intersection` claims
                           , claims <> claim
                           )
                        ) (disputedPoints, claimedPoints) circlesAroundSeed

                circlesAroundWithoutDispute =
                  fmap (S.\\ newDisputedPoints) circlesAroundSeed

                newFinitePatches =
                  M.unionWith (<>) finitePatches $
                  (circlesAroundWithoutDispute `M.restrictKeys` hull)

                remainingSeeds = growingSeeds circlesAroundWithoutDispute

largestFinitePatchArea :: Voronoi -> Int
largestFinitePatchArea Voronoi { finitePatches = patches } =
  maximum $ fmap length patches

parsePoint :: String -> Point
parsePoint str = Point (read x) (read y)
  where (x, _ : _ : y) = break (== ',') str

part1 :: String -> Int
part1 = largestFinitePatchArea . voronoi . S.fromList . fmap parsePoint . lines

main :: IO ()
main = do
  input <- readFile "input/day06"
  print (part1 input)

-- voronoi :: Set Point -> Voronoi
-- voronoi points = ()
--   where hull = quickHull points

--         go closedPatches growingPatches radius =
--           where claimedPatches = fmap (\p -> circle p radius)


-- claimable :: Grid -> Point -> Bool
-- claimable grid pt = M.lookup pt grid == Just False

-- claims :: Grid -> Map Point (Set Point) -> Map Point (Set Point)
-- claims grid collected =


--claims :: Grid a -> Map (Point a) [Point a] -> Map (Point a) [Point a]

-- data InfExt a = Infinite | Finite a

-- voronoi :: [Point a] -> Map (Point a) (InfExt a)
-- voronoi = undefined
--   where star (Point x y) = Point x (y +
