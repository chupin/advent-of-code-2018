{-# LANGUAGE OverloadedLists #-}

module Main where

import           Control.Applicative
import           Control.Monad
import           Data.Map            (Map)
import qualified Data.Map            as M
import           Data.Maybe
import           Data.Set            (Set)
import qualified Data.Set            as S
import           Text.Parsec

import           Debug.Trace

type Index = Int

data Point = Point { x :: Int
                   , y :: Int
                   }
           deriving(Eq, Ord, Show)

data Rect = Rect { rectTop :: Point
                 , rectBot :: Point
                 }
          deriving(Show)

asPointSet :: Rect -> Set Point
asPointSet r@(Rect (Point tx ty) (Point bx by)) =
  foldr S.insert [] [ Point px py
                    | px <- [tx..bx] :: [Int]
                    , py <- [ty..by] :: [Int]
                    ]

parseLine :: String -> Maybe (Index, Rect)
parseLine str =
  case parse parser "" str of
    Left _      -> Nothing
    Right input -> Just input
  where parser = do
          let int = read <$> some digit
          _ <- char '#'
          idx <- int
          spaces
          _ <- char '@'
          spaces
          tx <- int
          _ <- char ','
          ty <- int
          _ <- char ':'
          spaces
          sx <- int
          _ <- char 'x'
          sy <- int
          pure (idx, Rect { rectTop = Point tx ty
                          , rectBot = Point (tx + sx - 1) (ty + sy - 1)
                          })

area :: Rect -> Int
area (Rect (Point x1 y1) (Point x2 y2)) = (x2 - x1) * (y2 - y1)

isInside :: Point -> Rect -> Bool
isInside (Point x y) (Rect (Point tx ty) (Point bx by)) =
  x >= tx && x <= bx && y >= ty && y <= by

intersect :: Rect -> Rect -> Maybe Rect
intersect r1@Rect { rectTop = Point x1 y1
                  , rectBot = Point x2 y2
                  } Rect { rectTop = Point x3 y3
                         , rectBot = Point x4 y4
                         } =
  if ti `isInside` r1 && bi `isInside` r1
  then Just int
  else Nothing
  where ti = Point (max x1 x3) (max y1 y3)
        bi = Point (min x2 x4) (min y2 y4)

        int = Rect { rectTop = ti
                   , rectBot = bi
                   }

makeChart :: Foldable t
          => t Rect
          -> Map Point Int
makeChart =
  foldr (\rect -> M.unionWith (+) (M.fromSet (const 1) (asPointSet rect))) []

allIntersectionPoints :: Map Index Rect
                      -> Set Point
allIntersectionPoints rects =
  M.foldrWithKey (\idx rect ipts -> foldr (go rect) ipts (M.delete idx rects)) [] rects
  where go rect orect ipts =
          maybe ipts ((<> ipts) . asPointSet) (intersect rect orect)

part1 :: Map Index Rect -> Int
part1 = length . M.filter (>= 2) . makeChart

isLonely :: Foldable t => Rect -> t Rect -> Bool
isLonely rect rects = all (\orect -> isNothing (intersect rect orect)) rects

part2 :: Map Index Rect -> Maybe Index
part2 rects =
  M.foldrWithKey (\idx rect acc -> if isLonely rect (M.delete idx rects)
                                   then Just idx
                                   else acc) Nothing rects

main :: IO ()
main = do
  input <- readFile "input/day03"
  case foldM (\acc line -> do
                 (idx, sqr) <- parseLine line
                 pure (M.insert idx sqr acc)) [] (lines input) of
    Nothing -> error "Parsing the input failed :("
    Just rects -> do
      print (part1 rects)
      print (part2 rects)
