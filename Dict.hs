{-# LANGUAGE OverloadedLists, TypeFamilies #-}

module Dict ( Dict(..)
            , insert
            , member
            , lookupTails
            , deleteTails
            ) where

import           Data.List (intercalate)
import           Data.Map  (Map)
import qualified Data.Map  as M
import           GHC.Exts

-- A dictionnary data structure to lay out a bunch of words in a
-- compact manner. Every word starting with the same character shares
-- the same tail. Nothing indicates the end of a word.
data Dict c = Dict (Map c (Maybe (Dict c)))

instance (Show c, Ord c) => Show (Dict c) where
  show (Dict dict) =
    intercalate "," (fmap (\(k, v) -> show k ++ " -> [" ++ show v ++ "]")
                     (M.toList dict))

instance Ord c => Semigroup (Dict c) where
  Dict d1 <> Dict d2 = Dict (M.unionWith (<>) d1 d2)

instance Ord c => Monoid (Dict c) where
  mempty = Dict []

instance Ord c => IsList (Dict c) where
  type Item (Dict c) = [c]

  fromList = foldr insert mempty
  toList (Dict dict) =
    M.foldrWithKey (\c dict acc -> fmap (c :) (toList dict) ++ acc) [] dict

-- Insert a word in the dictionnary
insert :: Ord c => [c] -> Dict c -> Dict c
insert [] dict = dict
insert (c : cs) (Dict dict) =
  Dict (M.insertWith (<>) c (insert cs []) dict)

delete :: Ord c => [c] -> Dict c -> Dict c
delete [] dict = dict
delete [c] (Dict dict) =
  case M.lookup c dict of
    Just [] -> Dict (M.delete c dict)
    _ -> Dict dict
delete (c : cs) (Dict dict) =
  case M.lookup c dict of
    Just subDict ->
      case delete cs subDict of
        Dict subDict ->
          case M.lookup c subDict of
            Just [] ->


      where dict = delete cs dict


-- Is a word in the dictionnary? Assumes the empty word is always in
-- it
member :: Ord c => [c] -> Dict c -> Bool
member [] _ = True
member (c : cs) (Dict dict) =
  case M.lookup c dict of
    Nothing   -> False
    Just dict -> cs `member` dict

lookupTails :: Ord c => c -> Dict c -> Maybe (Dict c)
lookupTails c (Dict d) = M.lookup c d

deleteTails :: Ord c => c -> Dict c -> Dict c
deleteTails c (Dict d) = Dict (M.delete c d)
