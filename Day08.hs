{-# LANGUAGE DeriveFoldable, DeriveFunctor, DeriveTraversable #-}

module Day08 where

import Control.Monad
import Parser

import Debug.Trace

data Tree a = Node [a] [Tree a]
            deriving(Show, Functor, Foldable, Traversable)

parseTree :: Parser Int (Tree Int)
parseTree = do
  childCount <- any
  metaCount <- any
  children <- replicateM childCount parseTree
  metadata <- replicateM metaCount next
  pure (Node metadata children)

part1 :: String -> Maybe Int
part1 = fmap sum . runParser parseTree . fmap read . words

value :: Tree Int -> Int
value (Node meta []) = sum meta
value (Node meta children) = sum (fmap select meta)
  where childCount = length children
        childrenValue = fmap value children
        select idx
          | idx <= childCount = childrenValue !! (idx - 1)
          | otherwise = 0

part2 :: String -> Maybe Int
part2 = fmap value . runParser parseTree . fmap read . words

main :: IO ()
main = do
  input <- readFile "input/day08"
  print (part1 input)
  print (part2 input)
